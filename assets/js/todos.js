//check Off todos by clicking
$("ul").on("click", "li", function () {
	$(this).toggleClass("completedTask");
});

//delete todo by clicking
$("ul").on("click", "span", function(event) {
	$(this).parent().fadeOut(300, function() {
		$("this").remove()
	});
	//stops bubbling up other elements
	event.stopPropagation();	
});

//adding new todo to the list
$("input[type='text']").keypress(function(event){
	//checking if Enter is pressed
	if(event.which === 13) {
		//checking if input is not empty
		if($(this).val() !== ""){
			var newToDo = $(this).val();
			$("ul").append("<li><span><i class=\"far fa-trash-alt\"></span></i> " + newToDo + "</li>");
			$(this).val("");
		}
	}
});

$(".fa.fa-plus").click(function (event) { 
	$("input[type='text']").fadeToggle(300);
});